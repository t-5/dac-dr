import math
import random
import signal
import sys
from datetime import datetime
from multiprocessing import Array, Lock, Value
from multiprocessing.pool import Pool
from time import time


# constants ############################################################################################################

R_TARGET = 2000.0 # Ohms

MEASUREMENT_UNCERTAINTY = 0.05 # Ohms

PROCESSES = 8

Rs = [
    #1997.1,
    #1997.4,
    #1997.2,
    #1996.5,
    #1996.6,
    #1997.3,
    #1996.8,
    #1997.5,
    #1997.4,
    #1997.5,
    #1997.0,
    #1997.3,
    #1997.2,
    #1997.0,
    #1996.9,
    #1997.5,

    1996.9,
    1997.3,
    1997.5,
    1996.6,
    1997.2,
    1997.2,
    1997.3,
    1996.8,
    1997.1,
    1996.3,
    1996.7,
    1996.5,
    1996.5,
    1996.8,
    1996.6,
    1996.3,
]

NEEDED_RS = 16

R_IDX_MAPPING = {
    0:  1001,
    1:  1002,
    2:  1003,
    3:  1004,
    4:  1005,
    5:  1006,
    6:  1007,
    7:  1008,
    8:  1011,
    9:  1012,
    10: 1013,
    11: 1014,
    12: 1015,
    13: 1016,
    14: 1017,
    15: 1018,
}


# needed resistors check ###############################################################################################

if len(Rs) < NEEDED_RS:
    print(f"need more than {NEEDED_RS} resistors")
    exit(1)


# globals ##############################################################################################################

start = time()


# helper functions #####################################################################################################

def r_par(r1, r2):
    """ return calculation for two resistors in parallell """
    return r1 * r2 / (r1 + r2)


def print_best_rs(rs, deviation, run, prev_best_deviation, r_idxs, start):
    """ print a single best found result """
    print()
    print()
    print(f"Run:                     {run.value}")
    elapsed = time() - start
    print(f"Runtime:                 {elapsed:.2f}s")
    print(f"Calculations/s:          {run.value / elapsed:.2f}")
    print(f"Deviation:               {deviation * 1000000:.1f} ppm")
    print(f"Previous best deviation: {prev_best_deviation * 1000000:.1f} ppm")
    improvement = abs(prev_best_deviation - deviation) * 1000000
    print(f"Improvement:             {improvement:.1f} ppm")
    print("Resistors:")
    for i in range(0, NEEDED_RS):
        print(f"  R{R_IDX_MAPPING[i]}: index {r_idxs[i] + 1:2d} => {rs[i]:.4f} Ohms")
    sys.stdout.flush()


def init_pool_processes(the_lock, the_start, the_Rs, the_best_rs, the_running, the_optimizations, the_runs):
    """ Initialize each process with a global variable lock etc... """
    global lock, start, Rs, best_rs, running, optimizations, runs
    lock = the_lock
    start = the_start
    Rs = the_Rs
    best_rs = the_best_rs
    running = the_running
    optimizations = the_optimizations
    runs = the_runs


def processRunner(run):
    """ run calculations in a single process """
    try:
        while running.value:
            # assemble random list of Rs to test
            rs_temp = Rs.copy()
            rs = []
            r_idxs = []
            seen = {}
            while len(rs) < NEEDED_RS:
                idx = random.randint(0, len(rs_temp) - 1)
                while idx in seen:
                    idx = random.randint(0, len(rs_temp) - 1)
                seen[idx] = True
                rs.append(rs_temp[idx])
                r_idxs.append(idx)

            # calculate deviation of positive side of LPF
            r1001 = rs[0]
            r1002 = rs[1]
            r1003 = rs[2]
            r1004 = rs[3]
            r1005 = rs[4]
            r1006 = rs[5]
            r1007 = rs[6]
            r1008 = rs[7]
            r1011 = rs[8]
            r1012 = rs[9]
            r1013 = rs[10]
            r1014 = rs[11]
            r1015 = rs[12]
            r1016 = rs[13]
            r1017 = rs[14]
            r1018 = rs[15]
            tmp_deviation = 0
            deviation = abs(1 - (r_par(r1001, r1011) / r_par(r1007, r1015)))
            tmp_deviation = max(deviation, tmp_deviation)
            deviation = abs(1 - (r_par(r1003, r1013) / r_par(r1005, r1016)))
            tmp_deviation = max(deviation, tmp_deviation)
            deviation = abs(1 - (r_par(r1012, r1004) / r_par(r1006, r1008)))
            tmp_deviation = max(deviation, tmp_deviation)
            deviation = abs(1 - (r_par(r1014, r1002) / r_par(r1017, r1018)))
            tmp_deviation = max(deviation, tmp_deviation)
            tmp_deviation = tmp_deviation * 4
            with best_rs.get_lock():
                if tmp_deviation < best_deviation.value:
                    for i in range(0, NEEDED_RS):
                        best_rs[i] = rs[i]
                    optimizations.value += 1
                    print_best_rs(rs, tmp_deviation, runs, best_deviation.value, r_idxs, start)
                    best_deviation.value = tmp_deviation
                runs.value += 1
    except (KeyboardInterrupt, OSError):
        running.value = 0
    return run


class CalculatorPool:

    @staticmethod
    def run():
        print("=" * 78)
        print("Start:", datetime.fromtimestamp(start).isoformat())
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = Pool(PROCESSES, initializer=init_pool_processes, initargs=(lock, start, Rs, best_rs, running, optimizations, runs))
        signal.signal(signal.SIGINT, original_sigint_handler)
        with pool as tmp_pool:
            while running.value:
                tmp_pool.map(processRunner, range(1000000)) # do one million calculations in N processes at a time
            tmp_pool.close()
            tmp_pool.join()


# main program #########################################################################################################
if __name__ == '__main__':
    # lock and process shared variables
    lock = Lock()
    best_deviation = Value("d", 10000000000000)  # most pessimistic prediction possible :)
    best_rs = Array("d", [0] * NEEDED_RS)
    running = Value("I", 1)
    optimizations = Value("L", 0)
    runs = Value("L", 0)
    # run the calculator pool of processes
    try:
        cPool = CalculatorPool()
        cPool.run()
    except KeyboardInterrupt:
        pass
    # print final statistics
    print()
    print()
    exhaustion = runs.value / math.factorial(NEEDED_RS) * 100.0
    exhaustion_inverse = math.factorial(NEEDED_RS) / runs.value
    print(f"Total runtime:                 {time() - start:.2f}s")
    print(f"Calculations:                  {runs.value}")
    print(f"Calculations/s:                {runs.value / (time() - start):.0f}")
    print(f"Final Optimizations:           {optimizations.value}")
    print(f"Optimization space exhaustion: {exhaustion:.6f} % (1/{exhaustion_inverse:.0f})")

    # find real uncertainty
    r1001 = best_rs[0] + MEASUREMENT_UNCERTAINTY
    r1002 = best_rs[1] - MEASUREMENT_UNCERTAINTY
    r1003 = best_rs[2] + MEASUREMENT_UNCERTAINTY
    r1004 = best_rs[3] - MEASUREMENT_UNCERTAINTY
    r1005 = best_rs[4] + MEASUREMENT_UNCERTAINTY
    r1006 = best_rs[5] - MEASUREMENT_UNCERTAINTY
    r1007 = best_rs[6] + MEASUREMENT_UNCERTAINTY
    r1008 = best_rs[7] - MEASUREMENT_UNCERTAINTY
    r1011 = best_rs[8] + MEASUREMENT_UNCERTAINTY
    r1012 = best_rs[9] - MEASUREMENT_UNCERTAINTY
    r1013 = best_rs[10] + MEASUREMENT_UNCERTAINTY
    r1014 = best_rs[11] - MEASUREMENT_UNCERTAINTY
    r1015 = best_rs[12] + MEASUREMENT_UNCERTAINTY
    r1016 = best_rs[13] - MEASUREMENT_UNCERTAINTY
    r1017 = best_rs[14] + MEASUREMENT_UNCERTAINTY
    r1018 = best_rs[15] - MEASUREMENT_UNCERTAINTY
    tmp_deviation = 0
    deviation = abs(1 - (r_par(r1001, r1011) / r_par(r1007, r1015)))
    tmp_deviation = max(deviation, tmp_deviation)
    deviation = abs(1 - (r_par(r1003, r1013) / r_par(r1005, r1016)))
    tmp_deviation = max(deviation, tmp_deviation)
    deviation = abs(1 - (r_par(r1012, r1004) / r_par(r1006, r1008)))
    tmp_deviation = max(deviation, tmp_deviation)
    deviation = abs(1 - (r_par(r1014, r1002) / r_par(r1017, r1018)))
    tmp_deviation = max(deviation, tmp_deviation)
    tmp_deviation = tmp_deviation * 4
    print()
    print(f"Estimated real deviation:      {deviation * 1000000:.1f} ppm")
    print(f"Worst case CMRR expectec:      {20 * math.log10(deviation):.1f} dB")

