#!/usr/bin/python3

Vin = 3.3
Vout = 2.5

# E-series Rs ####################################################################################
N = 48 # E-Range for resistors
E_RESISTOR_SERIES = []
for decade in (1000, 10000):
    for m in range(0, N):
        factor = round((10 ** m) ** (1 / N), 2)
        E_RESISTOR_SERIES.append(factor * decade)

# Main program ###################################################################################

div = Vout / Vin
maxDev = 9999999

resDiv = 0
resR1 = 0
resR2 = 0

for R1 in E_RESISTOR_SERIES:
    for R2 in E_RESISTOR_SERIES:
        tDiv = R2 / (R2 + R1)
        dev = abs(tDiv - div)
        if dev < maxDev:
            resR1 = R1
            resR2 = R2
            maxDev = dev
            resDiv = tDiv

print("target div: %0.4f" % div)
print("actual div: %0.4f" % resDiv)
print("target V: %0.4f" % Vout)
print("actual V: %0.4f" % (Vin * resDiv))
print("R1: %d" % resR1)
print("R2: %d" % resR2)
